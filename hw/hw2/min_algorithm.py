import numpy as np


def lineSearchBacktrack(objFunc, x_k, f_k, p_k, descent_k, init_alpha, suff_decrease_c1, rho, tol, status):
    # initialize step size
    alpha_k = init_alpha

    # Compute trial point and objective value at trial point
    x_trial = x_k + alpha_k*p_k
    f_trial = objFunc.value(x_trial)
    num_func_it = 1

    # Search acceptable step size using backtracking line search
    while f_trial > f_k + suff_decrease_c1*alpha_k*descent_k:
        alpha_k = alpha_k*rho
        if alpha_k < tol:
            break
        x_trial = x_k + alpha_k*p_k
        f_trial = objFunc.value(x_trial)
        num_func_it += 1

    # set flag to indicate that the step size has become zero
    if alpha_k < tol:
        status = -3

    return x_trial, f_trial, alpha_k, num_func_it, status


def gd(x_start, objFunc, options=None):
    # initialize iteration counter
    iter_count = 0

    # initialize return flag (set to -99 so that we do not accidentally
    # declare success.)
    status = -99

    if options is None:
        options = {}
        options['max_iter'] = 1e4
        options['tol'] = 1.
        options['init_alpha'] = 1.
        options['output_level'] = 0
        options['rho_contraction'] = 0.5
        options['suff_decrease_c1'] = 1e-8

    # get parameters out put the options dictionary
    max_iter = options['max_iter']
    tol = options['tol']
    init_alpha = options['init_alpha']
    output_level = options['output_level']
    rho_contraction = options['rho_contraction']
    suff_decrease_c1 = options['suff_decrease_c1']

    # initialize current iterate
    x_k = np.copy(x_start)

    # initialize current function value, gradient, infinity norm of the
    # gradient
    f_k = objFunc.value(x_k)
    grad_k = objFunc.gradient(x_k)
    norm_grad_k = np.linalg.norm(grad_k, np.inf)

    # initialize counter for function evaluations
    num_func_evals = 1

    # determine how many variables are in the problem
    num_vars = len(x_start)

    # initialize search direction and its length to zero
    p_k = np.zeros(num_vars)
    norm_pk = 0.0

    # initialize descent condition to zero
    descent_k = 0.0

    # initialize step size to zero (for print only)
    alpha_k = 0.0

    # initialize the counter for the function evaluations needed in a
    # particular iteration
    num_func_it = 0

    # Print header and zero-th iteration for output
    if output_level >= 2:
        output_header = '%6s %23s %9s %9s %6s %10s' % \
                ('iter', 'f', '||p_k||', 'alpha', '#func', '||grad_f||')
        print(output_header)
        print('%6i %23.16e %9.2e %9.2e %6i %10.2e' %
              (iter_count, f_k, norm_pk, alpha_k, num_func_it, norm_grad_k))

    ###########################
    # Beginning of main Loop
    ###########################

    # We continue the main loop until the termination tolerance is met.
    while 1:
        # Compute search direction
        p_k = -grad_k/np.linalg.norm(grad_k)

        # Update descent condition
        descent_k = np.dot(grad_k, p_k)

        # Check termination tests
        if norm_grad_k < tol:
            # Termination tolerance met
            status = 0
            break

        if iter_count >= max_iter:
            status = -1
            break

        if descent_k >= 0:
            status = -2
            break

        # initialize step size
        alpha_k = init_alpha

        # Compute trial point and objective value at trial point
        x_trial, f_trial, alpha_k, num_func_it, status = \
            lineSearchBacktrack(objFunc, x_k, f_k, p_k, descent_k, init_alpha,
                                suff_decrease_c1, rho_contraction,
                                tol, status)


        # Update iterate
        x_k = np.copy(x_trial)
        f_k = f_trial

        # Compute gradient and its norm at the new iterate
        grad_k = objFunc.gradient(x_k)
        norm_grad_k = np.linalg.norm(grad_k, np.inf)

        # For the output, compute the norm of the step
        norm_pk = np.linalg.norm(p_k, np.inf)

        # Update counter for total number of function evaluations
        num_func_evals += num_func_it

        # Increase the iteration counter
        iter_count += 1

        # Iteration output
        if output_level >= 2:
            # Print the output header every 10 iterations
            if iter_count % 10 == 0:
                print(output_header)
            print('%6i %23.16e %9.2e %9.2e %6i %10.2e' %
                  (iter_count, f_k, norm_pk, alpha_k, num_func_it, norm_grad_k))

    ###########################
    # Finalize results
    ###########################

    # Set last iterate as the one that is returned, together with its objective
    # value
    x_sol = x_k
    f_sol = f_k

    # Set the statistics
    stats = {}
    stats['num_iter'] = iter_count
    stats['norm_grad'] = norm_grad_k
    stats['num_func_evals'] = num_func_evals

    # Final output message
    if output_level >= 1:
        print('')
        print('Final solution..................: %s' % np.array2string(x_sol))
        print('Final objective.................: %g' % f_sol)
        print('||grad|| at final point.........: %g' % norm_grad_k)
        print('Number of iterations............: %d' % iter_count)
        print('Number of function evaluations..: %d' % num_func_evals)
        print('')
        if status == 0:
            print('Exit: Critical point found.')
        elif status == -1:
            print('Exit: Maximum number of iterations (%d) exceeded.' %
                  iter_count)
        elif status == -2:
            print('Exit: Search direction is not a descent direction.')
        elif status == -3:
            print('Exit: Step size becomes zero.')
        else:
            print('ERROR: Unknown status value: %d\n' % status)

    # Return output arguments
    return status, x_sol, f_sol, stats
