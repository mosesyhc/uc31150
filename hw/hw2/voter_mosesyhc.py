import numpy as np
from scipy.stats import norm
from min_algorithm import gd


class voterModel:
    """Implement the negative log posterior, with a Gaussian prior and a probit likelihood."""

    def __init__(self, L, gamma, data):
        """Initialize object with Laplacian matrix, and gamma, the noise term standard deviation."""
        self._L = L
        self._gamma = gamma
        lamb, _ = np.linalg.eig(L)
        idx = lamb.argsort()
        lamb = lamb[idx]
        self._c = L.shape[0] / (lamb[1:] ** (-1)).sum()
        self._indy = data  # (index, y)

    def value(self, u):
        """Compute value of objective function at u, with data uy (columns: u, y)."""
        L = self._L
        gamma = self._gamma
        c = self._c
        indy = self._indy
        z = (u[indy[:, 0]] * indy[:, 1]) / gamma
        val = 1 / (2 * c) * u.T @ L @ u - np.log(norm.cdf(z)).sum() + 1 / 2 * (u ** 2).sum()
        return val

    def gradient(self, u):
        """Compute gradient at u, with data uy (columns: u, y)."""
        L = self._L
        gamma = self._gamma
        c = self._c
        indy = self._indy
        z = u[indy[:, 0]] * indy[:, 1] / gamma
        grad = 1 / c * L @ u + u
        grad[indy[:, 0]] -= norm.pdf(z) / norm.cdf(z)
        return grad


def weight(X):
    """return weight matrix."""
    expon = -((X[np.newaxis, :, :] - X[:, np.newaxis, :])**2).sum(axis=2) / 6
    W = np.exp(expon)
    return W


def laplacian(W):
    """Return Laplacian."""
    D = np.diag(W.sum(axis=1))
    L = D - W
    return L


X = np.loadtxt('weights_sorted.txt', delimiter=',')

# (a) compute weights matrix
W = weight(X[:, 1:])

# (b) graph laplacian
L = laplacian(W)

# classifcation with Fiedler
lamb, v = np.linalg.eig(L)
idx = lamb.argsort()
lamb = lamb[idx]
v = v[:, idx]

dempred = -v[:, 1] < 0
dem = X[:, 0] < 0

classacc = (dempred == dem).mean()

# (c) MAP estimator
demind = np.array([12, 31, 38]) - 1
repind = np.array([370, 322, 274]) - 1
data = np.hstack((np.vstack((demind, np.array((-1, -1, -1)))),
                  np.vstack((repind, np.array((1, 1, 1)))))).T

gamma = 0.1
u0 = -v[:, 1]

objf = voterModel(L, gamma, data)
status, x_sol, f_sol, stats = gd(u0, objf)

u_sol = x_sol
dempred_gd = u_sol < 0
classacc_gd = (dempred_gd == dem).mean()


# (d) Importance sampling
def impsamp(N, lamb, q, c):
    """Implement importance sampling sampler."""
    m = q.shape[0]
    lamb[np.abs(lamb) < 10**(-8)] = 0
    A = c ** (-0.5) * q @ np.diag(lamb ** (0.5))

    u = np.random.normal(size=(N, m)) @ A

    z = u[:, data[:, 0]] * data[:, 1] / gamma
    g = np.product(norm.cdf(z), axis=1)
    w = g / g.sum()

    return w, u


c = L.shape[0] / (lamb[1:] ** (-1)).sum()

allupost = []
alluvar = []
allclassacc = []
alless = []
for n in [10, 20, 50, 100, 200, 1000]:
    np.random.seed()
    w, u = impsamp(n, lamb, v, c)
    upost = w.T @ u
    uvar = ((w ** 2)[:, np.newaxis] * (u - upost) ** 2).sum(axis=0)

    dempred = -upost < 0
    classacci = (dempred == dem).mean() # np.max((dempred == dem).mean(), 1 - (dempred == dem).mean())

    allupost.append(upost)
    alluvar.append(uvar)
    allclassacc.append(classacci)

    # (e) effective sample size
    ess = 1 / (w**2).sum()
    alless.append(ess)