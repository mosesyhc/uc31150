import numpy as np
import seaborn as sns
from itertools import product
import matplotlib.pyplot as plt

class ising:
    """Construct Ising model."""

    def __init__(self, th):
        self._th = th

    def condprob(self, i, x):
        """Return conditional probability for x_i = 1."""
        th = self._th
        alph = 2 * (th[i] * x).sum()
        pr = np.exp(alph) / (np.exp(alph) + 1)
        return pr


def gibbs(x0, model, N, Nburn):
    p = x0.size
    assert p == model._th.shape[0]

    xset = np.array((1, -1))

    sample = np.zeros((N+Nburn, p))
    sample[0] = x0
    for n in np.arange(1, N+Nburn):
        xnow = np.copy(sample[n - 1])
        for j in np.arange(p):
            pr = model.condprob(j, xnow)
            xnow[j] = np.random.choice(xset, p=np.array((pr, 1 - pr)))
        sample[n] = xnow

    return sample[Nburn:]


def logprior(th):
    logprior = -np.abs(th).sum() / lamb
    return logprior


def alph(th, x):
    val = 0
    for i in np.arange(p):
        for j in np.arange(i+1, p):
            val += th[i, j] * x[i] * x[j]
    return val


def Z(th):
    allx = np.array([i for i in product([-1, 1], repeat=p)])

    alphs = np.array([alph(th, x) for x in allx])
    const = np.exp(alphs).sum()
    return const


def isinglogpost(thv, X, lamb):
    if (thv < -1).any() or (thv > 1).any():
        return -np.inf
    th = np.zeros((p, p))
    th[np.triu_indices(p, 1)] = thv
    th[np.tril_indices(p, -1)] = th.T[np.tril_indices(p, -1)]

    n = X.shape[0]
    logpost = - n * np.log(Z(th)) + logprior(th)
    for i in np.arange(n):
        logpost += alph(th, X[i])

    return logpost


def normprop(curr, s2=0.1):
    p = curr.shape[0]
    propnext = curr + np.random.normal(0, np.sqrt(s2), p)
    return propnext


def mh_ising(th0, X, pi, prop, lamb, s2, N, Nburn):
    samples = np.zeros((N + Nburn, th0.shape[0]))

    samples[0] = th0
    for i in np.arange(1, N + Nburn):
        sampnext = normprop(samples[i - 1], s2)
        if np.log(np.random.uniform()) < np.min(pi(sampnext, X, lamb) -
                                                pi(samples[i - 1], X, lamb), 0):
            samples[i] = np.copy(sampnext)
        else:
            samples[i] = np.copy(samples[i - 1])

    return samples[Nburn:]


p = 5
th = np.array((0, 0.5, 0, 0.5, 0,
               0.5, 0, 0.5, 0, 0.5,
               0, 0.5, 0, 0.5, 0,
               0.5, 0, 0.5, 0, 0,
               0, 0.5, 0, 0, 0)).reshape((p, p))


model = ising(th)
x0 = np.array((1, -1, -1, 1, 1))

xsamp = gibbs(x0, model, N=1000, Nburn=5000)
sampmean = xsamp.mean(axis=0)
sampvar = xsamp.var(axis=0)

R = np.corrcoef(xsamp.T)
fig, ax = plt.subplots(1, 1, figsize=(5, 4))
sns.heatmap(R, cmap='Blues', ax=ax)
plt.savefig('ising_corr.png', dpi=150)


# metropolis-hastings
lamb = 0.2
# th0 = th[np.triu_indices(5, 1)].flatten()  #test
th0 = np.random.laplace(0, lamb, size=int(p * (p-1) / 2))
thsamp = mh_ising(th0, xsamp, isinglogpost, normprop, lamb=0.2, s2=0.1, N=1000, Nburn=10000)
