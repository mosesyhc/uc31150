"""A Gaussian Experiment with Metropolis-Hastings, HW3."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def gen_multinormal(N, m, Sigma):
    d = m.shape[0]
    Z = np.random.normal(size=(N, d))
    X = m + Z @ np.linalg.cholesky(Sigma).T
    return X


def logpost(rho, X):
    n = X.shape[0]
    if rho >= 1 or rho <= -1:
        logpost = -np.inf
    else:
        logpost = -(n + 1) / 2 * np.log(1 - rho**2) - 1 / (2 * (1 - rho**2)) * (X[:, 0]**2 - 2 * rho * np.prod(X, axis=1) + X[:, 1]**2).sum()

    return logpost


def mh_simple(rho0, X, pi, N, Nburn, step=0.1):
    samples = np.zeros(N + Nburn)
    runmean = np.zeros(N + Nburn)
    runvar = np.zeros(N + Nburn)

    samples[0] = rho0
    runmean[0] = rho0
    runvar[0] = 0
    for i in np.arange(1, N + Nburn):
        rho_next = np.random.uniform(samples[i - 1] - step, samples[i - 1] + step)
        if np.log(np.random.uniform()) < np.min(pi(rho_next, X) - pi(samples[i - 1], X), 0):
            samples[i] = rho_next
        else:
            samples[i] = samples[i - 1]
        runmean[i] = 1 / (i + 1) * (i * runmean[i - 1] + samples[i])
        runvar[i] = 1 / i * ((i - 1) * runvar[i - 1] + (samples[i] - runmean[i])**2)

    return samples, runmean, runvar


def plot(runmeans, runvars):
    fig, ax = plt.subplots(2, 1, figsize=(6, 6))
    runmeans.plot(color=['r', 'b'], style=['-', '--'],
                  alpha=0.75,
                  ylabel='running mean',
                  ax=ax[0])
    runvars.plot(color=['r', 'b'], style=['-', '--'],
                 alpha=0.6,
                 xlabel='iteration', ylabel='running variance',
                 ax=ax[1])
    plt.tight_layout()
    plt.savefig('mh_plot.png', dpi=150)
    plt.show()
    return


# generate data
N = 1000
m = np.zeros(2)
rho = 0.
Sigma = np.array((1, rho, rho, 1)).reshape((2, 2))
X = gen_multinormal(N, m, Sigma)

# Metropolis-Hastings
rho0 = 0.1
Nburn = 10000

samples = []
runmeans = []
runvars = []
steps = [0.1, 0.4]
for step in steps:
    sample, runmean, runvar = mh_simple(rho0, X, logpost, N, Nburn, step)
    samples.append(sample[Nburn:])
    runmeans.append(runmean[:Nburn])
    runvars.append(runvar[:Nburn])
runmeans = pd.DataFrame(np.array(runmeans).T, columns=steps)
runvars = pd.DataFrame(np.array(runvars).T, columns=steps)

# plot
plot(runmeans, runvars)
