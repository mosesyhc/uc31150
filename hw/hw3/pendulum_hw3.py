"""Pendulum example."""
import numpy as np
import matplotlib.pyplot as plt


def pendulum_step(v0, dt, delta):
    """Step forward in pendulum example."""
    v1 = np.zeros_like(v0)
    v1[0] = v0[1] * dt + v0[0]
    v1[1] = - dt * np.sin(v0[0]) + (1 - delta * dt) * v0[1]

    return v1


def pendulum(T, v0, delta, gamma, n=1000):
    """Simulate pendulum."""
    d = v0.shape[0]
    v = np.zeros((n+1, d))

    dt = T / n

    # v = (u, w)
    v[0] = v0
    for i in np.arange(n):
        v[i + 1] = pendulum_step(v[i], dt, delta)
    y = v + np.random.normal(scale=np.sqrt(gamma), size=(n+1, d))

    return y, v


def threeDVAR(T, m0, y, K, H, delta, n=1000):
    """Perform 3D-VAR on the pendulum example."""
    d = m0.shape[0]
    m = np.zeros((n+1, d))
    I_d = np.diag(np.ones(d))

    dt = T / n

    m[0] = m0
    for i in np.arange(n):
        m[i + 1] = (I_d - np.outer(K, H)) @ pendulum_step(m[i], dt, delta) + \
            K * y[i + 1]

    return m


n = 20
dt = 0.2
T = n * dt
delta = 0.1
gamma = 0.1 ** 2
v0 = np.array((np.pi/4, 0))
d = v0.shape[0]

ys, vs = pendulum(T, v0, delta, gamma, n)

timesteps = np.arange(0, T + 10 ** (-9), T / n)
fig, ax = plt.subplots(2, 1, figsize=(6, 6))
ax[0].plot(timesteps, ys[:, 0].T)
ax[0].set_ylabel('$y_u$')
ax[1].plot(timesteps, ys[:, 1].T)
ax[1].set_xlabel('$t$')
ax[1].set_ylabel('$y_w$')
plt.savefig('pendulum_obs.png', dpi=150)
plt.show()

etas = np.arange(0, 0.1, 0.001)
Nrun = 100
allresults_3dvar = np.zeros((etas.size, 3, Nrun))
for k in np.arange(Nrun):
    m0 = np.random.uniform(-np.pi/4, np.pi/4, 2)#v0 + np.random.normal(0, np.sqrt(gamma), 2)
    results_3dvar = np.zeros((etas.size, 3))
    for j, eta in enumerate(etas):
        H = np.array((1, 0))
        C = np.array((eta, 0, 0, 0)).reshape((d, d))
        S = H @ C @ H.T + gamma
        K = S**(-1) * C @ H.T

        ms_u = threeDVAR(T, m0, ys[:, 0], K, H, delta, n)

        results_3dvar[j, 0] = eta
        results_3dvar[j, 1] = (((ms_u - vs) ** 2).sum(axis=1) ** (1/2)).sum()

        H = np.array((0, 1))
        C = np.array((0, 0, 0, eta)).reshape((d, d))
        S = H @ C @ H.T + gamma
        K = S**(-1) * C @ H.T

        ms_w = threeDVAR(T, m0, ys[:, 1], K, H, delta, n)

        results_3dvar[j, 2] = (((ms_w - vs) ** 2).sum(axis=1) ** (1/2)).sum()
    allresults_3dvar[:, :, k] = results_3dvar
meanresults_3dvar = allresults_3dvar.mean(axis=2)
q90lresults_3dvar = np.quantile(allresults_3dvar, 0.05, axis=2)
q90uresults_3dvar = np.quantile(allresults_3dvar, 0.95, axis=2)

fig, ax = plt.subplots(1, 1)
ax.plot(etas, meanresults_3dvar[:, 1], label='$y_u$', color='b')
ax.plot(etas, q90lresults_3dvar[:, 1], linestyle='--', color='b')
ax.plot(etas, q90uresults_3dvar[:, 1], linestyle='--', color='b')
ax.plot(etas, meanresults_3dvar[:, 2], label='$y_w$', color='r')
ax.plot(etas, q90lresults_3dvar[:, 2], linestyle='--', color='r')
ax.plot(etas, q90uresults_3dvar[:, 2], linestyle='--', color='r')
ax.set_ylabel('$\sum_i ||v_i - m_i||_{\ell_2}$')
ax.set_xlabel(r'$\theta$')
fig.legend(loc=9, ncol=2)
# plt.tight_layout()
plt.savefig('mse.png', dpi=150)


# particle filtering
def particle_filter(N, gamma, y):
    """Perform basic particle filter."""
    J = y.shape[0]
    pi = np.zeros((J, N, 2))
    v = np.zeros((J, N, 2))
    prob = np.zeros((J, N))
    ess = np.zeros(J)

    Sigma = gamma * np.diag(np.ones(2))
    L = np.linalg.cholesky(Sigma)
    P = np.linalg.inv(L)
    pi0 = v0 + np.random.normal(size=(N, 2)) @ L.T # np.random.uniform(-np.pi/4, np.pi/4, (N, 2))  # v0 + np.random.normal(size=(N, 2)) @ L.T #

    prob[0] = 1/N * np.ones(N)
    choose_ind = np.random.choice(np.arange(N), size=N,
                                  replace=True, p=prob[0])
    pi[0] = pi0[choose_ind]
    v[0] = pi0
    ess[0] = 1 / (prob[0] ** 2).sum()
    for j in np.arange(1, J):
        eps = np.random.normal(size=(N, 2)) @ L.T
        for n in np.arange(N):
            v[j, n] = pendulum_step(v[j - 1, n], dt, delta) + eps[n]
            prob[j, n] = np.exp(-0.5 * (y[j] - v[j, n]).T @
                                (P.T @ P) @ (y[j] - v[j, n]))
        prob[j] = prob[j] / prob[j].sum()
        choose_ind = np.random.choice(np.arange(N), size=N,
                                      replace=True, p=prob[j])
        pi[j] = v[j][choose_ind]
        ess[j] = 1 / (prob[j] ** 2).sum()

    return pi, prob, ess


Nparts = [5, 20, 50, 100]
results_particle = {}
fig, ax = plt.subplots(1, 1, figsize=(6,6))
for Npart in Nparts:
    results_particle[Npart] = particle_filter(Npart, gamma, ys)
    ax.plot(results_particle[Npart][2], label=Npart)
ax.set_xlabel('iteration')
ax.set_xticks(np.arange(0, 21, 5))
ax.set_ylabel('effective sample size')
fig.legend(loc=9, bbox_to_anchor=(0.5, 0.97), ncol=4)
# ax.set_yscale('log')
plt.tight_layout()
plt.savefig('pf_ess.png', dpi=150)