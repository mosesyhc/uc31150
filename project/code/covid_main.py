import numpy as np
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import seaborn as sns
from seirs import SEIRS
from particle_filter import particle_filter
sns.set_palette('Dark2')

def process_IL_covid_data():
    D = pd.read_table('time_series_covid19_confirmed_US.txt', delimiter=',')
    D_IL = D[D.Province_State.isin(['Illinois'])]
    D_IL_TOTAL = D_IL.sum(axis=0)[11:]
    D_IL_WAVE2 = D_IL_TOTAL[253:]

    return D_IL_TOTAL, D_IL_WAVE2


def plot_IL_covid():
    d, _ = process_IL_covid_data()
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    ax.plot(d / 100000)
    ax.set_xticks(d.index[0::60])
    ax.set_ylabel('confirmed cases (in hundred thousands)')
    plt.savefig('../tex/fig/il_total.png', dpi=150)


def get_IL_wave2_data():
    _, d = process_IL_covid_data()
    d = np.array(d, dtype=float)
    return d


def train_covid_model(Npart):
    y = get_IL_wave2_data() / 100000
    N = 12.67 * 10  # in thousands  (2019 estimate)
    kappa = 0.3
    gamma = 0.25
    p0 = np.array((0.9, 0.1, 0., 0.))

    th, w, ess = particle_filter(Npart, N, y, kappa, gamma, p0, resample=True)

    return th, w, ess


def run_covid_Npart():
    Nparts = [5, 25, 50, 100, 250, 500, 1000]
    res = {}
    for Npart in Nparts:
        res[Npart] = train_covid_model(Npart)
    return res


def ess_Npart(res):
    Nparts = [5, 25, 50, 100, 250, 500, 1000]
    ess_df = pd.DataFrame(columns=('index', 'Npart', 'value'))
    for Npart in Nparts:
        ess = res[Npart][2]
        ess_df = ess_df.append(pd.DataFrame(
            np.hstack((np.arange(ess.shape[0]).reshape(ess.shape[0], 1),
                       Npart * np.ones((ess.shape[0], 1)),
                       ess.reshape(ess.shape[0], 1))),
            columns=('index', 'Npart', 'value')))
    ess_df.Npart = ess_df.Npart.astype(str)

    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    sns.lineplot(x='index', y='value', hue='Npart', data=ess_df, legend=None)
    ax.set_yscale('log')
    ax.set_xlabel('days')
    ax.set_ylabel('effective sample size')
    plt.savefig('../tex/fig/covid_ess.png', dpi=150)
    return


def predict_traj(res_N):
    y = get_IL_wave2_data() / 100000
    N = 12.67 * 10  # in hundred thousands  (2019 estimate)
    kappa = 0.3
    p0 = np.array((0.9, 0.1, 0., 0.))

    nstep = 180
    trajectories = pd.DataFrame(columns=('index', 'S', 'E', 'I', 'R'))

    (th, w, ess) = res_N
    (J, Npart) = w.shape

    th_post = w[-1] @ th[-1]

    model = SEIRS(N, th_post, kappa, p0)
    model.simulate(nstep)

    trajs = model.get_traj()
    obs = model.get_obs()

    init_date = dt.date(2020, 10, 1)
    xticks = [(init_date + dt.timedelta(days=float(i))).strftime('%Y-%m-%d') for i in np.arange(nstep + 1)]

    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    model.plot_state()
    sns.scatterplot(x=xticks[:-1], y=obs, ax=ax, color='b', alpha=0.5, marker='.')
    sns.scatterplot(x=np.arange(J), y=y, ax=ax, color='k', marker='.')
    plt.axvline(x=J, ymin=0, ymax=np.max(trajs), linestyle=':', color='k', alpha=0.5)
    ax.set_xticks(xticks[::60])
    ax.set_ylabel('compartment size (in hundred thousands)')
    # plt.savefig('../tex/fig/trajpost.png', dpi=500)
    return

if __name__ == '__main__':
    res = train_covid_model(1000)
    pass
