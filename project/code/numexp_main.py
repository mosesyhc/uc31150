"""Main script."""

import numpy as np
from seirs import SEIRS
import matplotlib.pyplot as plt
import seaborn as sns
from particle_filter import sample_th, particle_filter

sns.set_palette('Dark2')


def plot_sample():
    """Plot a sample trajectory."""
    Npop = 100
    # sample figure for epidemic trajectory
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))

    th = sample_th(default=True)
    model = SEIRS(Npop, th)
    model.plot_state()

    ax.set_xticks(np.arange(0, 365 * 4 + 1, 365))
    ax.set_xlabel('iteration')
    ax.set_ylabel('compartment size (%)')
    plt.savefig('../tex/fig/seir.png', dpi=150)
    return


def main_pf(saveplot=False, plots=False, resample=False):
    """Run numerical examples for particle filter."""
    Npart = 50
    Npop = 100
    nstep = 365 * 2

    th = sample_th(default=True)
    model = SEIRS(Npop, th)
    model.simulate(nstep)

    y = model.get_obs()
    th_pf, w_pf, ess_pf = particle_filter(Npart, Npop, y, kappa=0.05, resample=resample)

    th_pfmean = w_pf[-1] @ th_pf[-1]

    if plots:
        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        model_pf = SEIRS(Npop, th_pfmean)
        model_pf.simulate(nstep)

        model.plot_state()
        model_pf.plot_state(add=True)

        ax.set_xticks(np.arange(0, nstep + 1, 365))
        ax.set_xlabel('days')
        ax.set_ylabel('compartment size (%)')
        if saveplot:
            plt.savefig('../tex/fig/seir_pf.png', dpi=150)

        fig, ax = plt.subplots(1, 1, figsize=(6, 6))
        sns.lineplot(x=np.arange(nstep), y=ess_pf)
        plt.axvline(np.argmax(y), 0, 100, linestyle=':', color='k', alpha=0.5)
        ax.set_xticks(np.arange(0, nstep + 1, 365))
        ax.set_xlabel('iteration')
        ax.set_ylabel('effective sample size')
        ax.set_yscale('log')
        if saveplot:
            plt.savefig('../tex/fig/ess_pf.png', dpi=150)
    return th_pf, w_pf, ess_pf


def run_numexp():
    th_noresample = []
    w_noresample = []
    ess_noresample = []
    for i in np.arange(5):
        th_i, w_i, ess_i = main_pf(resample=False)
        th_noresample.append(th_i)
        w_noresample.append(w_i)
        ess_noresample.append(ess_i)

    th_resample = []
    w_resample = []
    ess_resample = []
    for i in np.arange(5):
        th_i, w_i, ess_i = main_pf(resample=True)
        th_resample.append(th_i)
        w_resample.append(w_i)
        ess_resample.append(ess_i)
    return ess_noresample, ess_resample


def plot_ess_compare(ess_noresample, ess_resample):
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))

    for i in np.arange(5):
        essnoi = ess_noresample[i]
        sns.lineplot(x=np.arange(essnoi.shape[0]), y=essnoi, color='k', alpha=0.1)
        essi = ess_resample[i]
        sns.lineplot(x=np.arange(essi.shape[0]), y=essi, color='b', linestyle=':', alpha=0.1)
    ax.set_yscale('log')
    ax.set_ylabel('effective sample size')
    ax.set_xlabel('iteration')
    fig.savefig('../tex/fig/ess_compare.png', dpi=150)


if __name__ == '__main__':
    ess_noresample = np.load('ess_noresample.npy')
    ess_resample = np.load('ess_resample.npy')
    plot_ess_compare(ess_noresample, ess_resample)
    pass
