"""Particle filter implementation."""
import numpy as np
from seirs import SEIRS


def sample_th(n=1, default=False):
    """Sample th = (mu, beta, omega, sigma, gamma, alpha)."""
    if default:
        return np.array((1 / (76 * 365), 0.21, 1 / (365), 1/7, 1/14, 0))
    mu = np.random.uniform(1 / (90 * 365), 1 / (65 * 365), n)
    beta = np.random.beta(10, 40, n)
    omega = np.random.uniform(1 / 120, 1 / 30, n)
    sigma = np.random.uniform(1 / 14, 1 / 4, n)
    gamma = np.random.uniform(1 / 21, 1 / 7, n)
    alpha = np.random.beta(1, 200, n)

    th = np.column_stack((mu, beta, omega, sigma, gamma, alpha))
    return th


def residual_sampling_mod(N, th, w, y, kappa, Sigma, gamma):
    """Perform residual sampling with modification."""
    N_remain = np.floor(N * w)
    th_new = []
    for i in np.arange(N):
        for j in np.arange(N_remain[i]):
            th_new.append(th[i])

    wnew = (N * w - N_remain) / (N - N_remain.sum())
    if np.isnan(wnew).any() or (wnew < 0).any() or (wnew > 1).any():
        print(wnew)
        print('get here')
    wnew = wnew / wnew.sum()
    Nsamp = np.random.multinomial(N - N_remain.sum(), pvals=wnew)

    for i in np.arange(Nsamp.shape[0]):
        for j in np.arange(Nsamp[i]):
            th_tempprop = markov_kernel(th[i], Sigma, gamma)
            th_choose = metro_hast(N, j, th[i], th_tempprop, y, kappa)
            th_new.append(th_choose)

    th_new = np.array(th_new)

    return th_new


def markov_kernel(th, Sigma, gamma=0.025):
    """Generate new point via a Markov kernel."""
    nug = 10 ** (-8) * np.diag(np.ones(Sigma.shape[0]))
    Z = np.random.normal(size=th.shape)
    th_new = th + Z @ np.linalg.cholesky(gamma * Sigma + nug).T
    return th_new


def metro_hast(N, j, th_curr, th_prop, y, kappa):
    """Perform a single Metropolis-Hasting step."""
    model_curr = SEIRS(N, th_curr)
    model_curr.simulate(j)
    v_curr = model_curr.observe()

    model_prop = SEIRS(N, th_prop)
    model_prop.simulate(j)
    v_prop = model_prop.observe()

    if np.log(np.random.uniform()) < np.min(-0.5 / kappa *
                                            ((v_prop - y[j])**2 - (v_curr - y[j])**2), 0):
        return th_prop
    else:
        return th_curr


def resample_condition(ess, Npart):
    """Check resampling condition."""
    if ess < 0.1 * Npart:
        return True
    else:
        return False


def particle_filter(Npart, N, y, kappa, gamma=0.025, p0=np.array((0.9999, 0.0001, 0, 0)), resample=False):
    """Perform particle filter."""
    J = y.shape[0]
    th0 = sample_th(Npart)
    th = np.zeros((J, Npart, th0.shape[1]))  # parameter
    v = np.zeros((J, Npart))  # sampled observations
    w = np.zeros((J, Npart))  # weights
    ess = np.zeros((J))  # essential sample size

    w[0] = 1/Npart * np.ones(Npart)
    th[0] = th0
    ess[0] = 1 / (w[0] ** 2).sum()
    for j in np.arange(1, J):
        for n in np.arange(Npart):
            model = SEIRS(N, th[j - 1][n], kappa=kappa, p0=p0)
            model.simulate(j)
            v[:j, n] = model.get_obs()
        exponent = -0.5 / kappa ** 2 *(y[j] - v[j]) ** 2
        w[j] = np.exp(exponent - np.min(exponent))
        w[j] = w[j] / w[j].sum()
        ess[j] = 1 / (w[j] ** 2).sum()

        if resample:
            if resample_condition(ess[j], Npart):
                th_center = np.average(th[j - 1], axis=0, weights=w[j])
                Sigma = (w[j] * (th[j - 1] - th_center).T) @ (th[j - 1] - th_center) / (1 - (w[j] ** 2).sum())
                th[j] = residual_sampling_mod(Npart, th[j - 1], w[j], y, kappa, Sigma, gamma)

                w[j] = 1/Npart * np.ones(Npart)
                ess[j] = 1 / (w[j] ** 2).sum()

        ind = np.random.choice(np.arange(Npart), size=Npart,
                               replace=True, p=w[j])
        th[j] = th[j - 1][ind]

    return th, w, ess
