"""An SEIRS model for simulating epidemic."""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# initial probability
p0 = np.array((0.9999, 0.0001, 0, 0))
kappa = 0.05


class SEIRS:
    """SEIRS model."""

    def __init__(self, N, theta, kappa=kappa, p0=p0):
        self._dt = 1.
        self._N = N
        self._p = p0
        self._state = N * p0
        self._theta = theta
        self._kappa = kappa
        return

    def reset(self):
        self.__init__(self._N, self._theta, self._kappa, self._p)
        return

    def state(self):
        return self._state

    def get_traj(self):
        return self._traj

    def get_obs(self):
        if not hasattr(self, '_obs'):
            self.reset()
            _ = self.simulate(365 * 2)
        return self._obs

    def singlestep(self):
        (S, E, I, R) = self._state
        (mu, beta, omega, sigma, gamma, alpha) = self._theta
        dt = self._dt
        N = S + E + I + R
        Snew = (mu * N - beta * I * S / N + omega * R - mu * S) * dt + S
        Enew = (beta * I * S / N - sigma * E - mu * E) * dt + E
        Inew = (sigma * E - gamma * I - (mu + alpha) * I) * dt + I
        Rnew = (gamma * I - omega * R - mu * R) * dt + R

        self._state = (Snew, Enew, Inew, Rnew)
        return

    def step(self, nstep=1):
        for j in np.arange(nstep):
            self.singlestep()
        return

    def observe(self):
        kappa = self._kappa
        (_, E, _, _) = self._state
        y = np.max((0, kappa * E + np.random.normal(scale = 0.05 * kappa)))
        return y

    def simulate(self, niter):
        self.reset()
        state0 = self._state
        traj = np.zeros((niter, state0.shape[0]))
        obs = np.zeros(niter)

        traj[0] = state0
        obs[0] = self.observe()
        for i in np.arange(1, niter):
            self.step()
            traj[i] = self.state()
            obs[i] = self.observe()

        self._traj = traj
        self._obs = obs
        return traj, obs

    def plot_state(self, add=False):
        if not hasattr(self, '_traj'):
            self.reset()
            _ = self.simulate(365 * 2)
        traj = self._traj
        df = pd.DataFrame(traj, columns=('S', 'E', 'I', 'R'))
        if add:
            g = sns.lineplot(data=df, palette="Dark2", linewidth=1.5, alpha=0.25, legend=None)
        else:
            g = sns.lineplot(data=df, palette="Dark2", linewidth=1.5)
