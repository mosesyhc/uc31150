\section{\Large{\sffamily{Introduction}}}

Especially this past year in 2020, the number of studies reasonably skyrocketed around the topics of coronavirus disease, contagious disease transmission dynamics, and spread of an epidemic, in the wake of a global pandemic of COVID-19.  Specifically in the topic of disease transmission, a common approach is to model the disease through a compartmental model \citep{brauer2008compartmental}.  A crude search in Google Scholar service with the keywords ``COVID-19'' and ``compartmental model'' returns over 1,000 entries.  A compartmental model categorizes subgroups within a population by a certain feature and describes the dynamics between the subgroups.  The goal of this project is to use and explore extensions of data assimilation techniques expounded in lecture, in particular, the particle filter method, in application to the transmission disease compartmental model. 

\section{Transmission Disease Compartmental Model}\label{ssec:model}
A compartmental model describes the dynamics between subgroups of a population and therefore offers an attractive approach in understanding transmission disease spread, in which medical personnel, government authorities, public health workers, and researchers are concerned about the big picture so that stakeholders can make informed decisions.  For examples, for medical personnel, understanding the stress in a healthcare system caused by a surge of potentially infectious patients helps the reallocation of effort; for government authorities, in designing and implementing intervention strategies; for researcher, in studying the effectiveness of distribution of vaccines, and many more.

A general form of compartmental model, also the form employed in this project, consists of four subgroups where the number of individuals in each subgroup is recorded: Susceptible ($ S $), Exposed ($ E $), Infectious ($ I $), and Recovered ($ R $), and the total population is $N = S + E + I + R$ at any time.  The schema in Figure \ref{fig:seirs_model} depicts the model dynamics: 

\FloatBarrier
\begin{figure}[htp]
	\centering
	\includegraphics[width=0.6\columnwidth]{seirs_model.pdf}
	\caption{Schema for the SEIRS compartmental model.}
	\label{fig:seirs_model}
\end{figure}
\FloatBarrier

This ``open'' model is parameterized by $\theta = (\mu, \beta, \sigma, \gamma, \omega, \alpha),$ where each parameter is described by the following narrative: A susceptible individual, given there is an infectious individual in the population, will be exposed with a contact rate $\beta$ and remain in the Exposed group for $\sigma^{-1}$ latent period before becoming infectious, and stay in the Infectious group for $\gamma^{-1}$ recovery period before entering Recovered group.  Depending on the disease, a recovered individual can lose his immunity and return to the Susceptible group at a rate of $\omega$.  In addition to the inter-subgroup dynamics, this model is called ``open'' as it includes a birth-and-death rate $\mu$ where new individuals can be introduced into the population while current individuals can die and leave the population, and a death rate $\alpha$ from contracting the disease.  
This compartmental model is named the SEIRS model after the subgroup dynamics.

One inferred feature that is widely used in epidemiology is the basic reproduction number $R_0$, which is a constant defined to be the number of secondary infections caused by an infectious patient in a completely susceptible population.  The use of this metric often receives criticism for its over-simplification and the lack of considerations in other factors, such as intervention strategies.  This metric can be easily misinterpreted and misused \citep{delamater2019complexity}.  Other remedies have included the estimation of a time-varying $R(t)$ instead of a constant $R_0$ \citep{nishiura2009effective}.  In the described SEIRS model, the associated basic reproduction number is 
$$R_0 = \frac{\sigma}{\sigma + \mu} \times \frac{\beta}{\sigma + \mu + \alpha}. $$
In this study, we pay less primary emphasis on the interpretation of specific quantities of the model, but more on the solving of the inverse problem at hand.
\begin{remark}
    Other form of compartmental models have been employed to study the transmission dynamics of an epidemic.  Many extensions can be considered.  One approach is to introduce more subgroups to describe additional features, such as hospitalized, vaccinated, quarantined, etc.  One example that models the effect of quarantining is studied by \citet{amador2020stochastic}.  Another approach is to further breakdown the subgroups segregated by age and demographics such that each segregation can have a different parameterization depending on their specific dynamics, for example see \citet{birrell2011bayesian} for age segregation while introducing a contact pattern matrix to mimic cross-age-group dynamics.
\end{remark}

The mathematical description of the compartmental model is a simple system of difference equations:

\begin{align*}
	\frac{dS}{dt} &= \mu N - \beta \frac{IS}{N} + \omega R - \mu S, \\
	\frac{dE}{dt} &= \beta \frac{IS}{N} - \sigma E - \mu E, \\
	\frac{dI}{dt} &= \sigma E - \gamma I - (\mu+\alpha) I, \\
	\frac{dR}{dt} &= \gamma I - \omega R - \mu R.
\end{align*}
Hence this system is easy to simulate, given an appropriate initial condition $x_0 = (S_0, E_0, I_0, R_0), $ also described as $Np_0 = N\times (p_{S_0}, p_{E_0}, p_{I_0}, p_{R_0}),$ because the proportion of population in each subgroup is sometimes easier to capture.  A commonly chosen $p_0$ (before an epidemic occurs) is $(0.999, 0.001, 0, 0)$.  A typical time interval of updating the dynamics is 1 day.  Let $\Psi(\cdot)$ denote the forward model.  A particularly parameterized system is simulated and shown in Figure \ref{fig:seirs_traj}.  An important note here is to see the waves of the epidemic diminishing in amplitude as time advances.  This feature can only be seen for a system where $R_0 > 1$, and in equilibrium one may find a lingering nonzero proportion of population in the Exposed and Infectious subgroups.  For a detailed treatment of the recurrent epidemic wave and its implication, see \citet{bjornstad2002dynamics, stone2007seasonal, bjornstad2020seirs}.

\FloatBarrier
\begin{figure}[htp]
	\subfloat[Known parameterized SEIRS system for 4 years from the onset of epidemic, with parameter $\mu^{-1}=76$ years, $\beta=0.21$/day, $\omega^{-1}=1$ year, $\sigma^{-1}=7$ days, $\gamma^{-1}=14$ days, and $\alpha=0$. Initial condition is $p_0 = (0.999, 0.001, 0, 0)$. \label{fig:seirs_traj}]{\includegraphics[width=0.5\columnwidth]{seir}} 
	\subfloat[Estimated SEIRS system with posterior mean of $\theta$ obtained by particle filter method with noisy observations of a small proportion of the Exposed subgroup $\kappa E + \epsilon$. Faded lines are the true system dynamics. \label{fig:seirs_post}]{\includegraphics[width=0.5\columnwidth]{seir_pf}}
	\caption{Simulated and estimated SEIRS system.}
\end{figure}
\FloatBarrier

\section{Compartmental Model Posed as an Inverse Problem}
In this section we explore the connection between the compartmental model and the inverse problem.  In particular, following the chapter on particle filters in Chapters 7 \& 11 of \citet{sanz2018inverse}, we view the estimation of parameters in the said SEIRS model through the lens of a particle filter method.  Observations are required from the real system to estimate the model parameters.  Since the states of the model are by all means not directly observable, we describe the types of observations here.  Many sources of observations are typically available, including influence-like illness reports from hospital and clinic visits, test results of positivity of virus, etc.  The observation model follows closely to the observational model in \citet{birrell2011bayesian}.  However, for exploration purposes, we limit our observation data to the number of confirmed cases.  The number of confirmed cases are assumed to follow 
$$ Y_j = \kappa E_j + \epsilon_j, $$
where $Y_j$ is the number of confirmed cases and $E_j$ is the number of individuals in the Exposed subgroup at time $j$, $\kappa \in (0, 1]$ is the proportion of actual cases that can be observed, and $\epsilon_j$ is some i.i.d. Gaussian error.  We denote the observation at time $j$ as $y_j$ and the collection of observations as $\mathbf{Y}_j = \{y_1, \ldots, y_j \}$.  To estimate $\theta$ in the filtering problem means to estimate the posterior distributions $\pi_{j}(\theta) $ sequentially as more data are available.  According to Bayes rule, the posterior follows the relationship, at any time $j$, 
$$ \pi_j(\theta) = p(\theta | \mathbf{Y}_j) \propto \pi_0(\theta) p(\mathbf{Y}_j | \theta), $$
where $\pi_0(\theta)$ is the prior distribution for $\theta$.

Now we describe the process of sequentially approximating the target distribution by the particle filter.  As an example, the resulting trajectory from the posterior mean of $\theta$ is graphed in Figure \ref{fig:seirs_post}. Suppose we have a set of $m$ particles $\{\theta_j^{(1)},\ldots ,\theta_j^{(m)}\} $ at time $j$, with associated non-negative weights $\{w_j^{(1)}, \ldots, w_j^{(m)}\}.$  Then the posterior is approximated by 
$$\widehat{\pi}_j(\theta) = \sum_{k=1}^m w_j^{(k)} \delta(\theta - \theta_j^{(k)}),$$ 
where $\delta(\cdot)$ is the Dirac measure.  To approximate $\pi_{j+1}(\theta)$, if we use $\pi_j(\theta)$ as an importance sampling distribution, we note the following relationship between the weights and posterior distributions at time $j$ and time $j+1$, namely

$$ w^{(k)}_{j+1} \propto w^{(k)}_{j} \frac{\pi_{j+1}(\theta_j^{(k)})}{\pi_{j}(\theta_j^{(k)})} = w^{(k)}_{j} p(y_{j+1} | \theta_j^{(k)}).$$
Such relationship allows us to reweigh the particles with likelihood function evaluated at the new data observation.  As noted in class and homework problem related to the implementation of the particle filter method, as $j$ increases, many particles in the set receive practically no weight at all.  Two remedies are possible in this case, one is to naively increase particle size $m$ so that even a small proportion of particles is sufficient in estimating $\pi_j(\theta)$.  Obviously this is infeasible when $j$ is large and when the forward model is expensive to run and should remain for theoretical understanding, i.e. convergence of particle filter.  Next we describe the other remedy, which extends the content covered in lecture and attempts to solve the problem of particle degeneracy, namely the introduction of a resampling step in particle filter after reweighing the particles.

\section{Particle Filter Algorithm with Resampling Step}
The particle filter with resampling step seeks to remediate the problem of particle degeneracy.  In order to provide a remedy, we must first provide a measure for this degeneracy.  Following the suggestion in the related homework problem, the effective sample size is used to measure the degeneracy, and is defined as 
$$ \displaystyle \mathrm{ESS}(\{w_j^{(k)}\}_{k=1}^m) = \left(\sum_{k=1}^m w_j^{(k)^2}\right)^{-1},$$
where $w_j$ is standardized such that $\sum_{k=1}^m w_j^{(k)} = 1.$  The effective sample size ranges from $1$ to $m$ and denotes the number of particles that contributes ``significantly'' to estimation.  The key idea in this additional resampling step is to remove insignificant particles from the particle set, and to replace them with potentially ``good'' candidates as particles by resampling and move the remaining particles.  This was first introduced in \citet{gordon1993novel}, but further improved by \citet{gilks2001following} which uses a Markov kernel for moving the remaining particles.  This resampling step is triggered when the effective sample size drops below a certain threshold.  The particle filter algorithm for this inverse problem is described in Algorithm \ref{alg}.  


\begin{algorithm}
	\caption{Particle Filter with Resampling Step}
	\begin{algorithmic}[1]
		\STATE {\bf Input}: Prior distribution $\pi_0^m = \pi_0(\theta), $ number of particles $m$, initial condition $x_0$, real observations $\mathbf{Y}^0 = \{y^0_0, \dots, y^0_J\}$, resampling threshold coefficient $\rho$
		\STATE {\bf Particle Generation: } For $j=0,\ldots, J-1, $ perform
		\begin{enumerate}
			\item For  $k=1,\ldots,m$, 
			\subitem Draw $\theta_j^{(k)} \sim \pi_j^m$ i.i.d. 
			\subitem Step forward state dynamics $x_{j+1}^{(k)} = \Psi(x_j^{(k)})$. 
			\subitem Take observations $\mathbf{Y}_{j+1}(\theta_j^{(k)})$.
			\item Weigh particles $\bar{w}_{j+1}^{(k)} = \exp\left(-\frac{1}{2}|y^0_{j+1} - y_{j+1}(\theta_j^{(k)})|^2\right) $.
			\item Standardize $w_{j+1}^{(k)} = \bar{w}_{j+1}^{(k)}/\sum_{k=1}^m \bar{w}_{j+1}^{(k)}$. 
			\item {\bf If $\mathbf{ESS}(\{w_{j+1}^{(k)}\}_{k=1}^m) < \rho m$:} 
			\subitem Obtain $\{\theta_{j+1}^{(k)}\}_{k=1}^m $ and weights from resampling step [Algorithm \ref{alg:resamp}]. 
		\end{enumerate}
		\STATE {\bf Output: }  Approximate posterior distributions $\{\widehat{\pi}_j\}_{j=0}^J$. 
	\end{algorithmic}
	\label{alg}
\end{algorithm}
\FloatBarrier

\begin{remark}
	In the resampling step any resampling algorithm can be introduced, therefore going forward we single out this step and present a possible treatment of the resampling step.  The goal of the resampling is to restore the effective sample size by sampling around promising particles currently.  
\end{remark}

\subsection{Residual Resampling}
One choice of the resampling algorithm is called residual resampling \citep{liu1998sequential}.  The general idea is to round off the weights for insignificantly weighted particles, and reintroduce new particles by sampling from the remaining particles and move them slightly.  This idea is formalized in Algorithm \ref{alg:resamp}.  The use of residual resampling allows a consistency result and a central limit theorem result with a bounded limiting variance \citep{douc2005comparison}.

\begin{algorithm}
	\caption{Residual Resampling}
	\begin{algorithmic}[1]
		\STATE {\bf Input}: Number of particles $m$, weights $\{w^{(k)}\}_{k=1}^m$, and particles $\{\theta^{(k)}\}_{k=1}^m$.  
		\STATE {\bf Resampling: }
		\begin{enumerate}
			\item Set $\tilde{m}^{(k)} = \lfloor m w^{(k)} \rfloor,$ for $ k=1,\ldots,m.$
			\item Set $v^{(k)} = (mw^{(k)} - \tilde{m}^{(k)})/(m - \sum_{i=1}^m \tilde{m}^{(k)}). $
			\item Set $\{\bar{m}^{(k)}\}_{i=1}^m = \mathrm{MultN}(m - \sum_{i=1}^m \tilde{m}^{(k)}, \{v^{(k)}\}_{i=1}^m).$
			\item Set $\{\bar{\theta}^{(k)}\} $ be the samples of $\{\theta^{(k)}\}_{k=1}^m$ according to $\{\bar{m}^{(k)}\}_{i=1}^m$.
			\item Move each particle in $\{\bar{\theta}^{(k)}\} $ via a Metropolis-Hasting kernel.
			\item Remove particles from $\{\theta^{(k)}\}_{k=1}^m$ where $ \tilde{m}^{(k)} = 0 $.
			\item Set $\{\widehat{\theta}^{(k)}\}_{k=1}^m = \{\bar{\theta}^{(k)}\} \cup \{\theta^{(k)}\}.  $ 
		\end{enumerate}
		\STATE {\bf Output: }  New particles $\{\widehat{\theta}^{(k)}\}_{k=1}^m$ with weights $m^{-1}$. 
	\end{algorithmic}
	\label{alg:resamp}
\end{algorithm}

\section{Computational Results}
In this section we present the improvement results from adding the resampling step, first through the fully known parameterization of the SEIRS model, then through the observations of confirmed cases of COVID-19 in Illinois.  

\subsection{Prior distribution for $\theta$}
This subsection outlines the prior distribution used for the SEIRS model parameters.  All of the following has been scaled to the units of 1 day, which is the time interval used in simulating the SEIRS model dynamics in this project.
\begin{align*}
	\pi_0(\theta) & \propto \pi_0(\mu)\pi_0(\mu)\pi_0(\beta)\pi_0(\omega)\pi_0(\sigma)\pi_0(\alpha), \textrm{ where} \\
	\pi_0(\mu) &\sim \mathrm{Unif}[(90\cdot 365)^{-1}, (65\cdot 365)^{-1}], \\
	\pi_0(\beta) &\sim \mathrm{Beta}(10, 40), \\
	\pi_0(\omega) &\sim \mathrm{Unif}[120^{-1}, 30^{-1}], \\
	\pi_0(\sigma) &\sim \mathrm{Unif}[14^{-1}, 4^{-1}], \\
	\pi_0(\gamma) &\sim \mathrm{Unif}[21^{-1}, 7^{-1}], \textrm{ and}\\
	\pi_0(\alpha) &\sim \mathrm{Beta}(1, 200). \\
\end{align*}
The choice of prior provides a rather weak prior that covers a wide range of parameters. 
\subsection{Numerical Example}
The same parameterization is used for the SEIRS model, i.e. $\mu^{-1}=76$ years, $\beta=0.21$/day, $\omega^{-1}=1$ year, $\sigma^{-1}=7$ days, $\gamma^{-1}=14$ days, and $\alpha=0$.  In addition, we set initial condition $x_0 = (99.9, 0.1, 0, 0)$, observation coefficient $\kappa=0.05$ with observation error $\epsilon \sim N(0,0.5^2)$.  In terms of the hyperparameters, including the resampling threshold coefficient $\rho$ and the Metropolis-Hastings kernel used in the resampling, we have fixed them in the following way.  We set $\rho=0.1$, meaning when the effective sample size at any point in the algorithm drops below 10\% of the particle size, the resampling step is triggered and sometimes this step is called ``rejuvenation.''  For the Metropolis-Hastings kernel, we use the weighted sample covariance matrix of the particles $\{\theta^{(k)}\}_{k=1}^m$ with associated weights $\{w^{(k)}\}_{k=1}^m$, up to a multiplicative constant.  The weighted sample covariance matrix is defined to be 

$$ \Sigma_w(\theta) = \frac{1}{1 - \sum_{k=1}^m w^{(k)^2}} \sum_{k=1}^m w^{(k)} (\theta^{(k)} - \bar{\theta}) (\theta^{(k)} - \bar{\theta})^\mathsf{T}. $$

The model is advanced for a time period of 2 years.  Figure \ref{fig:ess_pf} illustrates a typical particle filtering run without the resampling step, where the effective sample size drops rapidly as soon as meaningful data come in and not able to recover.  Figure \ref{fig:ess_compare} illustrates the potential benefits in the additional resampling step.  Note that the effective sample size from running the algorithm with resampling step is generally larger and recovers to the full number of particles.  This is due to the added resampling step in the algorithm.  

\begin{figure}[htp]
	\subfloat[Particle size $m$ = 100. Time length $J$ = 4 years. Grey vertical line indicates the peak of the epidemic.  \label{fig:ess_pf}]{\includegraphics[width=0.5\columnwidth]{ess_pf}} 
	\subfloat[Particle size $m$ = 100. Time length $J$ = 2 years. Bottom grey lines denotes no resampling, top blue lines denotes with sampling, totaling 10 algorithm runs (5 without resampling, 5 with resampling). \label{fig:ess_compare}]{\includegraphics[width=0.5\columnwidth]{ess_compare}}
	\caption{Effective sample size and improvements with the additional resampling step.}
\end{figure}
\FloatBarrier

\begin{remark}
	In this numerical example, in addition to the hyperparameter aforementioned, the observation coefficient $\kappa$ and the variance in observation error are assumed to be known.  In a full estimation regime, one may choose to include these hyperparameters and the practically unknown quantities in observations into part of $\theta$.  
\end{remark}

\subsection{Application to COVID-19 Confirmed Cases in Illinois}
In Illinois, COVID-19 cases have been reported since the end of January, 2020.  Figure \ref{fig:covid_il} shows the number of confirmed cases in Illinois.  These observations are retrieved from \citet{dong2020interactive}, the COVID-19 tracking dashboard housed in John Hopkins University.  State-level observations are aggregated from county subtotals in the data.  The observations were scaled to have a unit of hundred thousand.  A quick note here is to point out the many levels of information available for analysis to provide insights to the ongoing pandemic.  Various intervention strategies have been incorporated in all levels of societies and physical gathering settings, and they have been evolving over time as well.  We chose to model the epidemic focusing on the latest surge in number of confirmed cases starting in October, 2020.  One perspective around this choice of modeling is to use the fact that the intervention strategies have more or less stabilized during this period of time.  The surge of cases surrounding the holiday gatherings recently provides a natural peak to trace.  

\begin{figure}[htp]
	\subfloat[Number of confirmed COVID-19 cases in Illinois.   \label{fig:covid_il}]{\includegraphics[width=0.5\columnwidth]{il_total}} 
	\subfloat[Simulated trajectory with posterior mean of $\theta$. Vertical grey line marks the end of observations.  Black markers denote the observations. Blue markers denote the simulated observations. \label{fig:trajpost}]{\includegraphics[width=0.5\columnwidth]{trajpost}}
	\caption{Particle filter applied to confirmed cases of COVID-19 in Illinois.}
\end{figure}

The total population of Illinois is estimated to be 12.79 million in 2019 \citep{national2018us}.  The initial condition at the beginning of the observations is unknown.  This is typical difficult to estimate, precisely what the main issue is with the inverse problem.  The observations suggests that the there should be a substantial number of individuals in the Susceptible subgroup and a significant amount in the Exposed group as well.  For this case study, we let $p_0$, the initial proportion at the beginning of this data set, be $(0.9, 0.08, 0.02, 0)$.  To determine the other parameters required to perform the analysis, a grid search was performed over a range of the resampling threshold coefficient, the multiplicative constant of the weighted covariance matrix in the Metropolis-Hastings kernel, the observation coefficient, and its error variance.  The best combination that produces a small mean squared error in observations and simulated observations is chosen.  The setting is as follows: threshold coefficient $\rho=0.1$, multiplicative constant = $0.25$, observation coefficient $\kappa=0.3$, and its error variance $\epsilon \sim N(0, 0.5^2)$.  

The particle filter with resampling step was applied with particle size $m=1000$. 

The result is shown as the trajectory simulated with the posterior mean obtained by the particle filter in Figure \ref{fig:trajpost}.  The simulated observations show that the observations generally match until middle of November, 2020.  If one considers closely the difference in the data and the simulation, we observe that the simulation predicted too early a peak in the Exposed subgroup, hence the early peak in simulation observations, while the data seem to be just starting to increase.  Another feature that shows this difference is from the derivatives of these observation curves.  We can see that the data curve seems to be convex and in the rise, where the simulated curve is concave and retreating after middle of November.  This potentially points to the considerations of using not only the number of cases as data, but the derivative information as well.  This is captured even by the media reports where the rate of change in number of cases/deaths and 14-day trends are also reported, for example \citet{nytimes2020}.  In a more systematic approach to set up a surveillance system to measure and estimate the higher derivative information, Northwestern University recently has also announced its hosting of a cross-institutional effort that measures the acceleration and jerk information of various observable statistics \citep{oehmke2020dynamic}.  A low-hanging fruit as improvement is to include death counts as data observations in the model, as these reported numbers directly inform about the death rate $\alpha$.  Another immediate potential improvement is to consider the observations be a proportion of both the Exposed and Infectious group, i.e. $\kappa_E E + \kappa_I I$, potentially even using asymptomatic and symptomatic case data as proxies to understand the observation mechanisms in the pandemic.

\section{Discussion and Bibliography}
For more details directly relevant to this case study, one should refer to \citet{bjornstad2020seirs} for the formulation of this specific variation of the disease transmission compartmental model, to \citet{birrell2011bayesian} for the formulation of the observation model, and to \citet{birrell2020efficient} for the implementation of a sequential Monte Carlo method, namely a particle filter with resampling step, for an epidemic.  

For understanding compartmental models used in public health, one should note the vast applications in different types of diseases, e.g. malaria \citep{yang2000malaria}, and HIV \citep{brandeau1993screening, zaric2000hiv}.  The fundamentals of compartmental modeling can be found in \citet{brauer2008compartmental}.  

In this project, we focus on only a branch of the family of what is called sequential Monte Carlo methods.  An introduction of such methods is seen in \citet{liu1998sequential, doucet2001introduction} and more recently a practical guide is captured in \citet{smith2013sequential}.  In particular, the ``resample-move'' strategy covered in this project and its in-depth analysis can be found in Chapter 6 of \citet{smith2013sequential}.  Other resampling strategies applied in the context of particle filtering are compared in \citet{douc2005comparison}.  One may also find a tutorial on particle filtering useful, especially in implementation \citet{doucet2000sequential, doucet2009tutorial}.  
There are more Bayesian approaches to solve this inverse problem.  Obviously, MCMC methods for directly sampling from the posterior distribution are among popular choices.  Examples of such are seen in \citet{cori2009temporal} for SARS in Hong Kong, \citet{birrell2011bayesian} for H1N1 in the United Kingdom, and mostly used for an offline retrospective analysis.  As we have alluded to in this project, the use of MCMC becomes highly infeasible in an online setting, i.e. filtering, when the likelihood evaluation requires the visiting of the entire data history each time the model acquires new data observations.  In addition to a sequential Monte Carlo method like particle filtering, an emulation strategy can also be employed to create a fast Gaussian process emulator to approximate the simulation model to eradicate the need for running the simulator always but retain the ability to predict and sample from an approximate posterior \citep{farah2014bayesian}.  A Bayesian approach to model an epidemic leveraging the time series correlation and allowing the incorporation of delayed case reporting can be seen in \citet{mcgough2020nowcasting}. 
Regarding the use of effective sample sizes, the calculation proposed in the project is possibly the simplest method considered in the literature.  Advances have been made to consider effective sample size according to a discrepancy measure \citep{martino2017effective}, in particular by considering $\ell_1, \ell_2$ distances between two probability mass functions.  Also, Kullback-Leibler and $\chi^2$-divergences, in addition to the total variation and the Hellinger distances, have also been studied to provide guidance in the conditions required on the necessary sample size in importance sampling \citep{sanz2018importance}, which has been shown crucial in particle filtering when approximating the next-step posterior distribution.
